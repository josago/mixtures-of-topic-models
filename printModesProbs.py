import matplotlib.pyplot as plt

import numpy as np

import re, sys

FLOAT_REGEX = "[\d\.+-einf]+"

def plotLine(filename, label, style):
    try:
        txt = open(filename, "r")

        lines = txt.readlines()

        txt.close()

        probs = []

        for line in lines[1 : ]:
            m = re.search("Documents with (\d+) items: Count = (\d+), p\(s = 0\) = ("+ FLOAT_REGEX + "), p\(s = 1\) = (" + FLOAT_REGEX + ")", line)
            
            if m is not None:
                groups = m.groups()
                
                (length, count, p0, p1) = (int(groups[0]), int(groups[1]), float(groups[2]), float(groups[3]))

                while len(probs) < length:
                    probs.append(0)
                
                probs.append(p1)

        plt.ylabel("Probability of selecting the LDA component")
        plt.xlabel("Number of items per container")

        plt.plot(range(1, len(probs)), probs[1 : ], label = label, color = "black", lw = 2, linestyle = style)
        plt.xlim(1, 40)
        #plt.yscale("log")
        plt.ylim(-0.1, 1.1)
    except IOError:
        pass

plt.subplot(2, 2, 1)
plt.title("K = 1 + 10")
 
plotLine("out/mixtures_4_recipes_K10.log",   "Recipes",   "-")
plotLine("out/mixtures_4_market_K10.log",    "Shop data", ":")
plotLine("out/mixtures_4_wikipedia_K10.log", "Wikipedia", "--")
 
plt.legend(loc = 1)
plt.grid(True)

plt.subplot(2, 2, 2)
plt.title("K = 2 + 20")
 
plotLine("out/mixtures_4_recipes_K20.log",   "Recipes",   "-")
plotLine("out/mixtures_4_market_K20.log",    "Shop data", ":")
plotLine("out/mixtures_4_wikipedia_K20.log", "Wikipedia", "--")
 
plt.legend(loc = 1)
plt.grid(True)

plt.subplot(2, 2, 3)
plt.title("K = 5 + 50")
 
plotLine("out/mixtures_4_recipes_K50.log",   "Recipes",   "-")
plotLine("out/mixtures_4_market_K50.log",    "Shop data", ":")
plotLine("out/mixtures_4_wikipedia_K50.log", "Wikipedia", "--")
 
plt.legend(loc = 1)
plt.grid(True)

plt.subplot(2, 2, 4)
plt.title("K = 10 + 100")
 
plotLine("out/mixtures_4_recipes_K100.log",   "Recipes",   "-")
plotLine("out/mixtures_4_market_K100.log",    "Shop data", ":")
plotLine("out/mixtures_4_wikipedia_K100.log", "Wikipedia", "--")
 
plt.legend(loc = 1)
plt.grid(True)

plt.show()