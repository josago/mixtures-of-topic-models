import re, sys

import numpy as np

import matplotlib.pyplot as plt

if __name__ == "__main__":
    csv = open(sys.argv[1], "r")
    
    counts    = []
    firstLine = True
    
    for line in csv:
        if firstLine:
            items = re.match("(\d+),(\d+),(\d+)", line).groups()
            
            print "Loaded CSV with " + items[0] + " documents, " + items[1] + " unique words and " + items[2] + " total items."
            
            firstLine = False
        else:
           count = len(line.split(","))
           
           counts.append(count)
    
    csv.close()
    
    plotData = np.bincount(counts)
    mode     = np.argmax(plotData)
    
    print "The mode is " + str(mode)
    
    plt.plot(plotData, color = "black", lw = 2)
    plt.ylabel("Number of recipes")
    plt.xlabel("Number of ingredients per recipe")
    plt.xlim(1, 25)
    plt.show()