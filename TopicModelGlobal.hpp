#ifndef TOPIC_MODEL_GLOBAL_HPP
#define TOPIC_MODEL_GLOBAL_HPP

#include "Tensor.hpp"
#include "TopicModel.hpp"

class TopicModelGlobal: public TopicModel
{
    protected:
        unsigned long K;
        
        float alpha0, beta;
        
        Tensor<unsigned long>* N_kw;
        Tensor<unsigned long>* N_k;
        
        Tensor<unsigned> gibbsSampler(const unsigned N, const unsigned* X, const unsigned samplesPerBatch, const bool train);
        
    public:
        TopicModelGlobal(const unsigned long K, const double alpha0, const double beta);
        
        ~TopicModelGlobal();
        
        void openCSV(const string filename);
        
        void train(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum);
        
        void testExtended(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum, unsigned long* casesNum, unsigned long** casesCount, float** perplexity, float** accuracy);
};

#endif