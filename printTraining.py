import matplotlib.pyplot as plt

import numpy as np

import re, sys

WIDTH  = 0.25
OFFSET = - WIDTH / 2 - WIDTH

limitX = 25 # Use 699 for Wikipedia, 25 for recipes and market.
aggreg = 1  # Use 25 for Wikipedia, 1 for recipes and market.
labels = ("Global topics (K = 1)", "LDA (K = 10)", "Mixture model (K = 1 + 10)")
colors = ("0.0",    "0.33",   "0.66")

txt = open(sys.argv[1], "r")

lines = txt.readlines()

txt.close()

model        = 0
counts       = []
perplexities = []
accuracies   = []

(fig, ax) = plt.subplots()

for line in lines[1 : ]:
    m = re.search("Documents with (\d+) items: Count = (\d+), perplexity = ([\de+-\.inf]+), accuracy = ([\de+-\.inf]+)", line)
    
    if m is not None:
        groups = m.groups()
        
        (length, count, perplexity, accuracy) = (int(groups[0]), int(groups[1]), float(groups[2]), float(groups[3]))
        
        while len(counts) < length:
            counts.append(0)
            perplexities.append(0)
            accuracies.append(0)
        
        counts.append(count)
        perplexities.append(perplexity)
        accuracies.append(accuracy)
    elif len(counts) > 0:
        accuracies = np.mean(np.reshape(accuracies[: limitX + 1], (aggreg, -1)), axis = 0)

        bars = ax.bar(np.arange(1, len(accuracies)) + OFFSET + WIDTH * model, 1 / accuracies.flatten()[1 : ], WIDTH, color = colors[model], label = labels[model])

        model       += 1
        counts       = []
        perplexities = []
        accuracies   = []

#plt.title("Wikipedia dataset")
#plt.xlabel("Words per document")
plt.title("Recipes dataset")
plt.xlabel("Ingredients per recipe")

plt.ylabel("Average per-item inverse accuracy")

#plt.yticks(np.arange(0, 2400, 100))
plt.yticks(np.arange(0, 500, 25))
 
plt.xticks(np.arange(1, limitX / aggreg + 1), np.arange(1, limitX / aggreg + 1) * aggreg)
plt.xlim(0, limitX / aggreg + 0.5)
 
plt.legend(loc = 1)
plt.tight_layout()
plt.grid(True)
plt.show()