SOURCES = BaselineRandom.cpp TopicModel.cpp TopicModelGlobal.cpp TopicModelLDA.cpp TopicModelMixture.cpp
OBJECTS = $(SOURCES:.cpp=.o)

# Performance flags:

CXX      = g++
CXXFLAGS = -std=c++11 -Wno-varargs -O2 -fopenmp

# Debug flags:

# CXX      = g++
# CXXFLAGS = -std=c++11 -Wno-varargs -g -D_DEBUG_MATH

all: mixtures

clean:
	rm -f *.o *.gch mixtures

mixtures: mixtures.cpp Tensor.hpp $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o mixtures mixtures.cpp $(OBJECTS)

%.o: %.cpp %.hpp
	$(CXX) $(CXXFLAGS) -c $<