#include "TopicModelMixture.hpp"

Tensor<unsigned> TopicModelMixture::gibbsSampler(const unsigned N, const unsigned* D, const unsigned* X, const unsigned samplesPerBatch, const bool train)
{
    // Randomly initialize the assignments to topics and modes:
    
    Tensor<unsigned> Z(2, 2, N);
    
    S = new Tensor<unsigned short>(1, D[N - 1] + 1);
    
    const unsigned K = K1 > K0 ? K1 : K0;
    
    Z.randint(0, K - 1);
    
    S->randint(TOPIC_MODEL_MIXTURE_GLOBAL, TOPIC_MODEL_MIXTURE_LDA);

    #pragma omp parallel for
    for (unsigned i = 0; i < N; i++)
    {
        unsigned       d = D[i];
        unsigned short m = (*S)(d);
        unsigned       k = Z(m, i);
        unsigned       w = X[i];
        
        if (train)
        {
            N_m->inc(m);
            N_mk->inc(m, k);
            N_mkw->inc(m, k, w);
        }
        
        N_d->inc(d);
        N_md->inc(m, d);
        N_mdk->inc(m, d, k);
        N_mdkw->inc(m, d, k, w);
    }
    
    // Gibbs sampler iteration:

    for (unsigned s = 0; s < samplesPerBatch; s++)
    {
//         cout << "\tSample #" + to_string(s + 1) + "...\n";

        #pragma omp parallel for
        for (unsigned d = 0; d <= D[N - 1]; d++)
        {
            const unsigned short mOld = (*S)(d);
            
            // Remove items from current mode:

            for (unsigned i = 0; i < N; i++)
            {
                if (D[i] == d)
                {
                    const unsigned k = Z(mOld, i);
                    const unsigned w = X[i];
                    
                    if (train)
                    {
                        N_m->dec(mOld);
                        N_mk->dec(mOld, k);
                        N_mkw->dec(mOld, k, w);
                    }

                    N_md->dec(mOld, d);
                    N_mdk->dec(mOld, d, k);
                    N_mdkw->dec(mOld, d, k, w);
                }
                else if (D[i] > d)
                {
                    break;
                }
            }
            
            // Sample the mode for each document:
            
            Tensor<float> logProb(1, 2);
            
            logProb(TOPIC_MODEL_MIXTURE_GLOBAL) = log(    pi0);
            logProb(TOPIC_MODEL_MIXTURE_LDA)    = log(1 - pi0);
            
            for (unsigned short m = TOPIC_MODEL_MIXTURE_GLOBAL; m <= TOPIC_MODEL_MIXTURE_LDA; m++)
            {
                logProb(m) -= lgammaf((*N_m)(TOPIC_MODEL_MIXTURE_GLOBAL) + (1 - m) * (*N_d)(d) + K0 * alpha0);
                logProb(m) -= lgammaf(m * (*N_md)(TOPIC_MODEL_MIXTURE_LDA, d) + K1 * alpha1);
                
                for (unsigned k0 = 0; k0 < K0; k0++)
                {
                    logProb(m) += lgammaf((*N_mk)(TOPIC_MODEL_MIXTURE_GLOBAL, k0) + (1 - m) * (*N_mdk)(TOPIC_MODEL_MIXTURE_GLOBAL, d, k0) + alpha0);
                    logProb(m) -= lgammaf((*N_mk)(TOPIC_MODEL_MIXTURE_GLOBAL, k0) + (1 - m) * (*N_mdk)(TOPIC_MODEL_MIXTURE_GLOBAL, d, k0) + numWords * beta0);
                    
                    for (unsigned w = 0; w < numWords; w++)
                    {
                        logProb(m) += lgammaf((*N_mkw)(TOPIC_MODEL_MIXTURE_GLOBAL, k0, w) + (1 - m) * (*N_mdkw)(TOPIC_MODEL_MIXTURE_GLOBAL, d, k0, w) + beta0);
                    }
                }

                for (unsigned k1 = 0; k1 < K1; k1++)
                {
                    logProb(m) += lgammaf(m * (*N_mdk)(TOPIC_MODEL_MIXTURE_LDA, d, k1) + alpha1);
                    logProb(m) -= lgammaf((*N_mk)(TOPIC_MODEL_MIXTURE_LDA, k1) + m * (*N_mdk)(TOPIC_MODEL_MIXTURE_LDA, d, k1) + numWords * beta1);
                    
                    for (unsigned w = 0; w < numWords; w++)
                    {
                        logProb(m) += lgammaf((*N_mkw)(TOPIC_MODEL_MIXTURE_LDA, k1, w) + m * (*N_mdkw)(TOPIC_MODEL_MIXTURE_LDA, d, k1, w) + beta1);
                    }
                }
            }
            
            // Sample mode:
            
            float u = log((float) rand() / RAND_MAX);
            
            unsigned short mNew = 0;
            
            float v = logProb(mNew);
            float s = logProb.sumLog();

            while (v - s < u)
            {
                mNew++;
                
                v += log1p(exp(logProb(mNew) - v));
            }
            
            // Assign mode:
            
            if (mNew >= 2)
            {
                cout << "Sampling error, randomizing mode assignment...\n";
                
                mNew = rand() / 2;
            }
            
            (*S)(d) = mNew;
            
            // Add items to the new mode:

            for (unsigned i = 0; i < N; i++)
            {
                if (D[i] == d)
                {
                    const unsigned k = Z(mNew, i);
                    const unsigned w = X[i];
                    
                    if (train)
                    {
                        N_m->inc(mNew);
                        N_mk->inc(mNew, k);
                        N_mkw->inc(mNew, k, w);
                    }

                    N_md->inc(mNew, d);
                    N_mdk->inc(mNew, d, k);
                    N_mdkw->inc(mNew, d, k, w);
                }
                else if (D[i] > d)
                {
                    break;
                }
            }
        }
        
        // Then sample all items within each document: 

        #pragma omp parallel for
        for (unsigned i = 0; i < N; i++)
        {
            const unsigned       d    = D[i];
            const unsigned short m    = (*S)(d);
            const unsigned       kOld = Z(m, i);
            const unsigned       w    = X[i];

            // De-assign item:

            if (train)
            {
                N_m->dec(m);
                N_mk->dec(m, kOld);
                N_mkw->dec(m, kOld, w);
            }

            N_md->dec(m, d);
            N_mdk->dec(m, d, kOld);
            N_mdkw->dec(m, d, kOld, w);
            
            // Calculate log-probabilities for the sampler:
            
            Tensor<float> logProb(1, K);
                
            if (m == TOPIC_MODEL_MIXTURE_GLOBAL)
            {
                for (unsigned v = 0; v < K0; v++)
                {
                    logProb(v) = log((*N_mk)(m, v) + alpha0) - log((*N_mk)(m, v) + numWords * beta0);
                    
                    for (unsigned w = 0; w < numWords; w++)
                    {
                        logProb(v) += log((*N_mkw)(m, v, w) + beta0);
                    }
                }
            }
            else if (m == TOPIC_MODEL_MIXTURE_LDA)
            {
                for (unsigned v = 0; v < K1; v++)
                {
                    logProb(v) = log((*N_mdk)(m, d, v) + alpha1) + log((*N_mkw)(m, v, w) + beta1) - log((*N_mk)(m, v) + numWords * beta1);
                }
            }
                    
            // Sample item:
            
            float u = log((float) rand() / RAND_MAX);
            
            unsigned k = 0;
            
            float v = logProb(k);
            float s = logProb.sumLog();

            while (v - s < u)
            {
                k++;
                
                v += log1p(exp(logProb(k) - v));
            }
            
            // Assign item:
            
            if (k >= K)
            {
                cout << "Sampling error, randomizing topic assignment...\n";
                
                k = rand() / K;
            }
            
            Z(m, i) = k;

            if (train)
            {
                N_m->inc(m);
                N_mk->inc(m, k);
                N_mkw->inc(m, k, w);
            }

            N_md->inc(m ,d);
            N_mdk->inc(m, d, k);
            N_mdkw->inc(m, d, k, w);
        }
    }
    
    return Z;
}

TopicModelMixture::TopicModelMixture(const unsigned K0, const unsigned K1, const float alpha0, const float alpha1, const float beta0, const float beta1, const float pi0)
{
    this->K0 = K0;
    this->K1 = K1;
    
    this->alpha0 = alpha0;
    this->alpha1 = alpha1;
    this->beta0  = beta0;
    this->beta1  = beta1;
    this->pi0    = pi0;
}

TopicModelMixture::~TopicModelMixture()
{
    if (csv.is_open())
    {
        csv.close();
    }
    
    delete N_m;
    delete N_mk; 
    delete N_mkw;
}

void TopicModelMixture::openCSV(const string filename)
{
    TopicModel::openCSV(filename);
    
    const unsigned K = K1 > K0 ? K1 : K0;

    N_m   = new Tensor<unsigned>(1, 2);
    N_mk  = new Tensor<unsigned>(2, 2, K);
    N_mkw = new Tensor<unsigned>(3, 2, K, numWords);
}

void TopicModelMixture::train(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum)
{
    const unsigned K = K1 > K0 ? K1 : K0;
    
    N_d    = new Tensor<unsigned>(1, batchSize);
    N_md   = new Tensor<unsigned>(2, 2, batchSize);
    N_mdk  = new Tensor<unsigned short>(3, 2, batchSize, K);
    N_mdkw = new Tensor<unsigned short>(4, 2, batchSize, K, numWords);
    
    for (unsigned long b = 0; b < batchNum; b++)
    {
        cout << "Batch #" + to_string(b + 1) + "...\n";

        N_d->zeros();
        N_md->zeros();
        N_mdk->zeros();
        N_mdkw->zeros();
        
        unsigned  N;
        unsigned* D = nullptr;
        unsigned* X = nullptr;
    
        loadCSV(batchSize, &N, &D, &X, nullptr, nullptr, nullptr);
        
        gibbsSampler(N, D, X, samplesPerBatch, true); // train = true
        
        delete S;
        
        free(D);
        free(X);
    }

    delete N_d;
    delete N_md;
    delete N_mdk;
    delete N_mdkw;
}

void TopicModelMixture::testExtended(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum, unsigned long* casesNum, unsigned long** casesCount, float** perplexity, float** accuracy)
{
    float* probMode1 = nullptr;
    
    *casesNum   = 0;
    *casesCount = nullptr;
    
    const unsigned K = K1 > K0 ? K1 : K0;
    
    N_d    = new Tensor<unsigned>(1, batchSize);
    N_md   = new Tensor<unsigned>(2, 2, batchSize);
    N_mdk  = new Tensor<unsigned short>(3, 2, batchSize, K);
    N_mdkw = new Tensor<unsigned short>(4, 2, batchSize, K, numWords);
    
    for (unsigned long b = 0; b < batchNum; b++)
    {
        cout << "Batch #" + to_string(b + 1) + "...\n";

        N_d->zeros();
        N_md->zeros();
        N_mdk->zeros();
        N_mdkw->zeros();
        
        unsigned  N;
        unsigned* D = nullptr;
        unsigned* X = nullptr;
        
        unsigned  Nbis;
        unsigned* Dbis = nullptr;
        unsigned* Xbis = nullptr;
    
        loadCSV(batchSize, &N, &D, &X, &Nbis, &Dbis, &Xbis);
        
        // Calculate the length of each container:
        
        for (unsigned long i = 0; i < N; i++)
        {
            (*N_d)(D[i])++;
        }
        
        for (unsigned long i = 0; i < Nbis; i++)
        {
            (*N_d)(Dbis[i])++;
        }
        
        Tensor<unsigned> Z = gibbsSampler(N, D, X, samplesPerBatch, false); // train = false
        
        // Enlarge data structures as needed:
        
        const unsigned long casesNumOld = *casesNum;
        
        *casesNum = N_d->max() + 1;
        
        *casesCount = (unsigned long*) realloc(*casesCount, *casesNum * sizeof(unsigned long));
        *perplexity = (float*)         realloc(*perplexity, *casesNum * sizeof(float));
        *accuracy   = (float*)         realloc(*accuracy,   *casesNum * sizeof(float));
        probMode1   = (float*)         realloc(probMode1,   *casesNum * sizeof(float));
        
        for (unsigned long l = casesNumOld; l < *casesNum; l++)
        {
            (*casesCount)[l] = 0;
            (*perplexity)[l] = 0.0;
            (*accuracy)[l]   = 0.0;
            
            probMode1[l]     = 0.0;
        }
        
        // Aggregate all partial metrics:
        
        #pragma omp parallel for
        for (unsigned long d = 0; d <= Dbis[Nbis - 1]; d++)
        {
            const unsigned short m = (*S)(d);
            const unsigned       l = (*N_d)(d);
            
            #pragma omp atomic
            probMode1[l] += m;
        }
        
        const unsigned long Nsum = (*N_m).sum();
        
        #pragma omp parallel for
        for (unsigned long i = 0; i < Nbis; i++)
        {
            const unsigned long  d = Dbis[i];
            const unsigned long  w = Xbis[i];
            const unsigned short m = (*S)(d);
            const unsigned       l = (*N_d)(d);
            
            float prob = 0.0;
            
            // TODO: Need to estimate the probabilities for m better:
            
            if (m == TOPIC_MODEL_MIXTURE_GLOBAL)
            { 
                for (unsigned k = 0; k < K0; k++)
                {
                    prob += (((*N_mkw)(m, k, w) + beta0) * ((*N_mk)(m, k) + alpha0)) / (((*N_mk)(m, k) + numWords * beta0) * (Nsum + K * alpha0));
                }
            }
            else if (m == TOPIC_MODEL_MIXTURE_LDA)
            {
                for (unsigned long k = 0; k < K1; k++) 
                {
                    prob += (((*N_mkw)(m, k, w) + beta1) / ((*N_mk)(m, k) + numWords * beta1)) * (((*N_mdk)(m, d, k) + alpha1) / (((*N_d)(d) + K * alpha1)));
                }
            }
            
            #pragma omp atomic
            (*casesCount)[l]++;
            
            #pragma omp atomic
            (*perplexity)[l] += log2(prob);
            
            #pragma omp atomic
            (*accuracy)[l]   += prob;
        }
        
        delete S;
        
        free(D);
        free(X);
        free(Dbis);
        free(Xbis);
    }
    
    // Get the final metrics:

    #pragma omp parallel for
    for (unsigned long l = 0; l < *casesNum; l++)
    {
        (*perplexity)[l]  = pow(2, - (*perplexity)[l] / (*casesCount)[l]);
        (*accuracy)[l]   /= (*casesCount)[l];
        probMode1[l]     /= (*casesCount)[l];
    }
    
    delete N_d;
    delete N_md;
    delete N_mdk;
    delete N_mdkw;
    
    // Print the mixture probabilities versus the document length:
    
    cout << "Mixtures probabilities:\n";
    
    for (unsigned long l = 0; l < *casesNum; l++)
    {
        if ((*casesCount)[l] > 0)
        {
            cout << "\tDocuments with " << l << " items: Count = " << (*casesCount)[l] << ", p(s = 0) = " << 1.0 - probMode1[l] << ", p(s = 1) = " << probMode1[l] << "\n";
        }
    }
    
    free(probMode1);
}