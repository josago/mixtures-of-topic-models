import sys

import numpy as np
import matplotlib.pyplot as plt

filename = sys.argv[1]

csv = open(filename, "r")

first = True

data     = np.zeros(20000, dtype = int)
data_max = 0

for line in csv:
    if first is False:
        length = len(line.split(","))
        
        data_max = max(data_max, length)
        
        data[length] += 1
    
    first = False

csv.close()

point_max = np.argmax(data)

print (point_max, data_max)

plt.figure(figsize = (8.0, 4.5))

plt.axvline(point_max, linestyle = "dotted", color = "black")

plt.xlabel("Number of words per document")
plt.ylabel("Number of documents")

plt.plot(data[: 501], color = "black", lw = 2)

plt.savefig(sys.argv[2], dpi = 300)