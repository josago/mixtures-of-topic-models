#include <cmath>
#include <iostream>

#include "TopicModelGlobal.hpp"

Tensor<unsigned> TopicModelGlobal::gibbsSampler(const unsigned N, const unsigned* X, const unsigned samplesPerBatch, const bool train)
{
    // Randomly initialize the assignments to topics:
    
    Tensor<unsigned> Z(1, N);
    
    Z.randint(0, K - 1);
    
    if (train)
    {
        #pragma omp parallel for
        for (unsigned i = 0; i < N; i++)
        {
            N_kw->inc(Z(i), X[i]);
            N_k->inc(Z(i));
        }
    }
    
    // Gibbs sampler iteration:

    for (unsigned s = 0; s < samplesPerBatch; s++)
    {
        cout << "\tSample #" + to_string(s + 1) + "...\n";

        #pragma omp parallel for
        for (unsigned i = 0; i < N; i++)
        {
            // De-assign item:
            
            if (train)
            {
                N_kw->dec(Z(i), X[i]);
                N_k->dec(Z(i));
            }
            
            // Calculate log-probabilities for the sampler:
            
#ifdef _DEBUG_MATH
            Tensor<float> logProbLong(1, K);
#endif
            
            Tensor<float> logProb(1, K);
            
            for (unsigned v = 0; v < K; v++)
            {
#ifdef _DEBUG_MATH
                logProbLong(v) = lgammaf(K * alpha0) - lgammaf((*N_k).sum() + K * alpha0);
                
                for (unsigned k = 0; k < K; k++)
                {
                    logProbLong(v) += lgammaf(numWords * beta) - lgammaf(alpha0);
                    
                    if (k == v)
                    {
                        logProbLong(v) += lgammaf((*N_k)(k) + 1 + alpha0) - lgammaf((*N_k)(k) + 1 + numWords * beta);
                        
                        for (unsigned w = 0; w < numWords; w++)
                        {
                            logProbLong(v) += lgammaf((*N_kw)(k, w) + 1 + beta) - lgammaf(beta);
                        }
                    }
                    else
                    {
                        logProbLong(v) += lgammaf((*N_k)(k) + alpha0) - lgammaf((*N_k)(k) + numWords * beta);
                        
                        for (unsigned w = 0; w < numWords; w++)
                        {
                            logProbLong(v) += lgammaf((*N_kw)(k, w) + beta) - lgammaf(beta);
                        }
                    }
                }
#endif
                
                logProb(v) = log((*N_k)(v) + alpha0) - log((*N_k)(v) + numWords * beta);
                
                for (unsigned w = 0; w < numWords; w++)
                {
                    logProb(v) += log((*N_kw)(v, w) + beta);
                }
            }
            
#ifdef _DEBUG_MATH
            for (unsigned v = 1; v < K; v++)
            {
                if ((logProb(v) > logProb(v - 1) and logProbLong(v) < logProbLong(v - 1)) or (logProb(v) < logProb(v - 1) and logProbLong(v) > logProbLong(v - 1)))
                {
                    cout << "Debug math error...\n";
                }
            }
#endif

            // Sample item:
            
            float u = log((float) rand() / RAND_MAX);
            
            unsigned k = 0;
            
            float v = logProb(k);
            float s = logProb.sumLog();

            while (v - s < u)
            {
                k++;
                
                v += log1p(exp(logProb(k) - v));
            }
            
            // Assign item:
            
            if (k >= K)
            {
                cout << "Sampling error, randomizing topic assignment...\n";
                
                k = rand() / K;
            }
            
            Z(i) = k;

            if (train)
            {
                N_kw->inc(Z(i), X[i]);
                N_k->inc(Z(i));
            }
        }
    }
    
    return Z;
}

TopicModelGlobal::TopicModelGlobal(const unsigned long K, const double alpha0, const double beta)
{
    this->K = K;
    
    this->alpha0 = alpha0;
    this->beta   = beta;
}

TopicModelGlobal::~TopicModelGlobal()
{
    if (csv.is_open())
    {
        csv.close();
    }
    
    delete N_kw;
    delete N_k;
}

void TopicModelGlobal::openCSV(const string filename)
{
    TopicModel::openCSV(filename);
    
    N_kw = new Tensor<unsigned long>(2, K, numWords);
    N_k  = new Tensor<unsigned long>(1, K);
}

void TopicModelGlobal::train(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum)
{
    for (unsigned b = 0; b < batchNum; b++)
    {
        cout << "Batch #" + to_string(b + 1) + "...\n";
        
        unsigned  N;
        unsigned* X = nullptr;
    
        loadCSV(batchSize, &N, nullptr, &X, nullptr, nullptr, nullptr);
        
        gibbsSampler(N, X, samplesPerBatch, true); // train = true
        
        free(X);
    }
}

void TopicModelGlobal::testExtended(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum, unsigned long* casesNum, unsigned long** casesCount, float** perplexity, float** accuracy)
{
    *casesNum   = 0;
    *casesCount = nullptr;
    
    Tensor<unsigned long> N_d(1, batchSize);
    
    for (unsigned b = 0; b < batchNum; b++)
    {
        cout << "Batch #" + to_string(b + 1) + "...\n";
            
        // Get a new batch:
        
        unsigned  N;
        unsigned* X = nullptr;
        unsigned* D = nullptr;
    
        loadCSV(batchSize, &N, &D, &X, nullptr, nullptr, nullptr);
        
//         Tensor<unsigned> Z = gibbsSampler(N, X, samplesPerBatch, false); // train = false
        
        const unsigned long Nsum = (*N_k).sum();
        
        // Calculate the number of items per document:
        
        N_d.zeros();

        for (unsigned i = 0; i < N; i++)
        {
            N_d(D[i])++;
        }
        
        // Enlarge data structures as needed:
        
        const unsigned long casesNumOld = *casesNum;
        
        *casesNum = N_d.max() + 1;
        
        *casesCount = (unsigned long*) realloc(*casesCount, *casesNum * sizeof(unsigned long));
        *perplexity = (float*)         realloc(*perplexity, *casesNum * sizeof(float));
        *accuracy   = (float*)         realloc(*accuracy,   *casesNum * sizeof(float));
        
        for (unsigned j = casesNumOld; j < *casesNum; j++)
        {
            (*casesCount)[j] = 0;
            (*perplexity)[j] = 0.0;
            (*accuracy)[j]   = 0.0;
        }
        
        // Aggregate all partial metrics:
        
        #pragma omp parallel for
        for (unsigned i = 0; i < N; i++)
        {
            const unsigned d = N_d(D[i]);
            
            float prob = 0.0;
            
            for (unsigned k = 0; k < K; k++)
            {
                prob += (((*N_kw)(k, X[i]) + beta) * ((*N_k)(k) + alpha0)) / (((*N_k)(k) + numWords * beta) * (Nsum + K * alpha0));
            }
            
            if (prob < 0)
            {
                cout << "Numerical error, calculated negative probability...\n";
            }
            else if (prob > 1)
            {
                cout << "Numerical error, calculated probability greater than one..." << prob << "\n";
            }
            
            #pragma omp atomic
            (*casesCount)[d]++;
            
            #pragma omp atomic
            (*perplexity)[d] += log2(prob);
            
            #pragma omp atomic
            (*accuracy)[d]   += prob;
        }
        
        free(X);
        free(D);
    }
    
    // Get the final metrics:
    
    #pragma omp parallel for
    for (unsigned d = 0; d < *casesNum; d++)
    {
        (*perplexity)[d] = pow(2, - (*perplexity)[d] / (*casesCount)[d]);
        (*accuracy)[d]   = (*accuracy)[d] / (*casesCount)[d];
    }
}