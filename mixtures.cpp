#include <iostream>

#include "BaselineRandom.hpp"
#include "TopicModelGlobal.hpp"
#include "TopicModelLDA.hpp"
#include "TopicModelMixture.hpp"

int main(const int argc, const char** argv)
{
    if (argc != 8)
    {
        cout << "Usage:\n";
        cout << "\tmixtures input.csv modelID numTopics batchSize trainingBatches testingBatches samplesPerBatch\n";
        
        return 1;
    }
    
    unsigned short modelID         = atoi(argv[2]);
    unsigned       numTopics       = atoi(argv[3]);
    unsigned long  batchSize       = atoi(argv[4]);
    unsigned long  trainingBatches = atoi(argv[5]);
    unsigned long  testingBatches  = atoi(argv[6]);
    unsigned long  samplesPerBatch = atoi(argv[7]);
    
    unsigned long  casesNum   = 0;
    unsigned long* casesCount = nullptr;
    
    float* perplexityExtended = nullptr;
    float* accuracyExtended   = nullptr;
    
    if (modelID == 0)
    {
        BaselineRandom model(false);
    
        model.openCSV(argv[1]);
        
        cout << "Training the model...\n";

        model.train(batchSize, samplesPerBatch, trainingBatches);
        
        cout << "Testing the model...\n";

        model.testExtended(batchSize, samplesPerBatch, testingBatches, &casesNum, &casesCount, &perplexityExtended, &accuracyExtended);
    }
    else if (modelID == 1)
    {
        BaselineRandom model(true);
    
        model.openCSV(argv[1]);
        
        cout << "Training the model...\n";

        model.train(batchSize, samplesPerBatch, trainingBatches);
        
        cout << "Testing the model...\n";

        model.testExtended(batchSize, samplesPerBatch, testingBatches, &casesNum, &casesCount, &perplexityExtended, &accuracyExtended);
    }
    else if (modelID == 2)
    {
        TopicModelGlobal model(numTopics, 1e-3, 1e-3);
    
        model.openCSV(argv[1]);
        
        cout << "Training the model...\n";

        model.train(batchSize, samplesPerBatch, trainingBatches);
        
        cout << "Testing the model...\n";

        model.testExtended(batchSize, samplesPerBatch, testingBatches, &casesNum, &casesCount, &perplexityExtended, &accuracyExtended);
    }
    else if (modelID == 3)
    {
        TopicModelLDA model(numTopics, 1e-3, 1e-3);
    
        model.openCSV(argv[1]);
        
        cout << "Training the model...\n";

        model.train(batchSize, samplesPerBatch, trainingBatches);
        
        cout << "Testing the model...\n";

        model.testExtended(batchSize, samplesPerBatch, testingBatches, &casesNum, &casesCount, &perplexityExtended, &accuracyExtended);
    }
    else if (modelID == 4)
    {
        TopicModelMixture model(numTopics / 10, numTopics, 1e-3, 1e-3, 1e-3, 1e-3, 0.5);
    
        model.openCSV(argv[1]);
        
        cout << "Training the model...\n";

        model.train(batchSize, samplesPerBatch, trainingBatches);
        
        cout << "Testing the model...\n";

        model.testExtended(batchSize, samplesPerBatch, testingBatches, &casesNum, &casesCount, &perplexityExtended, &accuracyExtended);
    }
    else
    {
        cout << "Use a model ID between 0 and 4.\n";
        
        return 2;
    }
    
    float Nall = 0;
    
    for (unsigned i = 0; i < casesNum; i++)
    {
        Nall += casesCount[i];
    }
    
    float perplexity = 0.0;
    float accuracy   = 0.0;
    
    for (unsigned i = 0; i < casesNum; i++)
    {
        if (casesCount[i] > 0)
        {
            perplexity += (casesCount[i] / Nall) * perplexityExtended[i];
            accuracy   += (casesCount[i] / Nall) * accuracyExtended[i];
        }
    }
    
    cout << "Perplexity = " << perplexity << ", accuracy = " << 1 / accuracy << "\n";

    for (unsigned i = 0; i < casesNum; i++)
    {
        if (casesCount[i] > 0)
        {
            cout << "\tDocuments with " << i << " items: Count = " << casesCount[i] << ", perplexity = " << perplexityExtended[i] << ", accuracy = " << 1 / accuracyExtended[i] << "\n";       
        }
    }
    
    free(casesCount);
    free(perplexityExtended);
    free(accuracyExtended);
    
    return 0;
}
