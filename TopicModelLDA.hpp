#ifndef TOPIC_MODEL_LDA_HPP
#define TOPIC_MODEL_LDA_HPP

#include "Tensor.hpp"
#include "TopicModel.hpp"

class TopicModelLDA: public TopicModel
{
    protected:
        unsigned K;
        
        float alpha1, beta;

        Tensor<unsigned>*      N_dk;
        Tensor<unsigned long>* N_kw;
        Tensor<unsigned long>* N_k;
        
        Tensor<unsigned> gibbsSampler(const unsigned N, const unsigned* D, const unsigned* X, const unsigned samplesPerBatch, const bool train);
        
    public:
        TopicModelLDA(const unsigned long K, const double alpha1, const double beta);
        
        ~TopicModelLDA();
        
        void openCSV(const string filename);
        
        void train(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum);
        
        void testExtended(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum, unsigned long* casesNum, unsigned long** casesCount, float** perplexity, float** accuracy);
};

#endif