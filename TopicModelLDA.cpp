#include <cmath>
#include <iostream>

#include "TopicModelLDA.hpp"

Tensor<unsigned> TopicModelLDA::gibbsSampler(const unsigned N, const unsigned* D, const unsigned* X, const unsigned samplesPerBatch, const bool train)
{
    // Randomly initialize the assignments to topics:
    
    Tensor<unsigned> Z(1, N);
    
    Z.randint(0, K - 1);
    
    #pragma omp parallel for
    for (unsigned i = 0; i < N; i++)
    {
        const unsigned d = D[i];
        const unsigned k = Z(i);
        const unsigned w = X[i];

        N_dk->inc(d, k);
        
        if (train)
        {
            N_kw->inc(k, w);
            N_k->inc(k);
        }
    }
    
    // Gibbs sampler iteration:

    for (unsigned s = 0; s < samplesPerBatch; s++)
    {
        cout << "\tSample #" + to_string(s + 1) + "...\n";

        #pragma omp parallel for
        for (unsigned i = 0; i < N; i++)
        {
            const unsigned d    = D[i];
            const unsigned kOld = Z(i);
            const unsigned w    = X[i];
            
            // De-assign item:

            N_dk->dec(d, kOld);
                
            if (train)
            {
                N_kw->dec(kOld, w);
                N_k->dec(kOld);
            }
            
            // Calculate log-probabilities for the sampler:
            
            Tensor<float> logProb(1, K);
            
            for (unsigned v = 0; v < K; v++)
            {
                logProb(v) = log((*N_dk)(d, v) + alpha1) + log((*N_kw)(v, w) + beta) - log((*N_k)(v) + numWords * beta);
            }
            
            // Sample item:
            
            float u = log((float) rand() / RAND_MAX);
            
            unsigned kNew = 0;
            
            float v = logProb(kNew);
            float s = logProb.sumLog();

            while (v - s < u)
            {
                kNew++;
                
                v += log1p(exp(logProb(kNew) - v));
            }
            
            // Assign item:
            
            if (kNew >= K)
            {
                cout << "Sampling error, randomizing topic assignment...\n";
                
                kNew = rand() / K;
            }
            
            Z(i) = kNew;
            
            N_dk->inc(d, kNew);
                
            if (train)
            {
                N_kw->inc(kNew, w);
                N_k->inc(kNew);
            }
        }
    }
    
    return Z;
}

TopicModelLDA::TopicModelLDA(const unsigned long K, const double alpha1, const double beta)
{
    this->K = K;
    
    this->alpha1 = alpha1;
    this->beta   = beta;
}

TopicModelLDA::~TopicModelLDA()
{
    if (csv.is_open())
    {
        csv.close();
    }
    
    delete N_kw;
    delete N_k;
}

void TopicModelLDA::openCSV(const string filename)
{
    TopicModel::openCSV(filename);

    N_kw  = new Tensor<unsigned long>(2, K, numWords);
    N_k   = new Tensor<unsigned long>(1, K);
}

void TopicModelLDA::train(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum)
{
    N_dk  = new Tensor<unsigned>(2, batchSize, K);
    
    for (unsigned long b = 0; b < batchNum; b++)
    {
        cout << "Batch #" + to_string(b + 1) + "...\n";

        N_dk->zeros();
        
        unsigned  N;
        unsigned* D = nullptr;
        unsigned* X = nullptr;
    
        loadCSV(batchSize, &N, &D, &X, nullptr, nullptr, nullptr);
        
        gibbsSampler(N, D, X, samplesPerBatch, true); // train = true
        
        free(D);
        free(X);
    }

    delete N_dk;
}

void TopicModelLDA::testExtended(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum, unsigned long* casesNum, unsigned long** casesCount, float** perplexity, float** accuracy)
{
    *casesNum   = 0;
    *casesCount = nullptr;
    
    N_dk  = new Tensor<unsigned>(2, batchSize, K);
    
    Tensor<unsigned long> N_d(1, batchSize);
    
    for (unsigned long b = 0; b < batchNum; b++)
    {
        cout << "Batch #" + to_string(b + 1) + "...\n";

        N_dk->zeros();
        
        unsigned  N;
        unsigned* D = nullptr;
        unsigned* X = nullptr;
        
        unsigned  Nbis;
        unsigned* Dbis = nullptr;
        unsigned* Xbis = nullptr;
    
        loadCSV(batchSize, &N, &D, &X, &Nbis, &Dbis, &Xbis);
        
        Tensor<unsigned> Z = gibbsSampler(N, D, X, samplesPerBatch, false); // train = false
        
        // Calculate the number of items per document:
        
        N_d.zeros();

        for (unsigned long i = 0; i < N; i++)
        {
            N_d(D[i])++;
        }
        
        for (unsigned long i = 0; i < Nbis; i++)
        {
            N_d(Dbis[i])++;
        }
        
        // Enlarge data structures as needed:
        
        const unsigned long casesNumOld = *casesNum;
        
        *casesNum = N_d.max() + 1;
        
        *casesCount = (unsigned long*) realloc(*casesCount, *casesNum * sizeof(unsigned long));
        *perplexity = (float*)         realloc(*perplexity, *casesNum * sizeof(float));
        *accuracy   = (float*)         realloc(*accuracy,   *casesNum * sizeof(float));
        
        for (unsigned long j = casesNumOld; j < *casesNum; j++)
        {
            (*casesCount)[j] = 0;
            (*perplexity)[j] = 0.0;
            (*accuracy)[j]   = 0.0;
        }
        
        // Aggregate all partial metrics:

        #pragma omp parallel for
        for (unsigned long i = 0; i < Nbis; i++)
        {
            const unsigned long d = N_d(Dbis[i]);
            
            float prob = 0.0;
            
            for (unsigned long k = 0; k < K; k++) 
            {
                prob += (((*N_kw)(k, Xbis[i]) + beta) / ((*N_k)(k) + numWords * beta)) * (((*N_dk)(Dbis[i], k) + alpha1) / ((N_d(Dbis[i]) + K * alpha1)));
            }
            
            #pragma omp atomic
            (*casesCount)[d]++;
            
            #pragma omp atomic
            (*perplexity)[d] += log2(prob);
            
            #pragma omp atomic
            (*accuracy)[d]   += prob;
        }
        
        free(D);
        free(X);
        free(Dbis);
        free(Xbis);
    }
    
    // Get the final metrics:
    
    #pragma omp parallel for
    for (unsigned long d = 0; d < *casesNum; d++)
    {
        (*perplexity)[d] = pow(2, - (*perplexity)[d] / (*casesCount)[d]);
        (*accuracy)[d]   = (*accuracy)[d] / (*casesCount)[d];
    }

    delete N_dk;
}