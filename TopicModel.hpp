#ifndef TOPIC_MODEL_HPP
#define TOPIC_MODEL_HPP

#include <string>
#include <fstream>

using namespace std;

class TopicModel
{
    protected:
        ifstream csv; // CSV file.
        
        unsigned long numDocs, numWords, numItems; // Basic dataset statistics.
        
    public:
        void openCSV(const string filename); // Open a CSV file.
        
        void loadCSV(const unsigned batchSize, unsigned* N, unsigned** D, unsigned** X, unsigned* Nbis, unsigned** Dbis, unsigned** Xbis); // Read a batch of documents from an opened CSV file.
        
        virtual void train(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum) = 0;
        
        void test(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum, float* perplexity, float* accuracy); // Test the model, aggregating all results globally.
        
        virtual void testExtended(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum, unsigned long* casesNum, unsigned long** casesCount, float** perplexity, float** accuracy) = 0; // Test the model, aggregating all results by number of items per document.
};

#endif