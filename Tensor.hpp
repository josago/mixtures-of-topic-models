#ifndef TENSOR_HPP
#define TENSOR_HPP

#include <chrono>
#include <random>
#include <cstdint>
#include <cstdarg>
#include <iostream>

using namespace std;

template <class T> class Tensor
{
    private:
        unsigned long  dims, elems;
        unsigned long* shape;
        
        T* values;
        
        Tensor() {}
    public:
        Tensor(const unsigned long dims, ...)
        {
            this->dims  = dims;
            this->shape = (unsigned long*) malloc(dims * sizeof(unsigned long));

            elems = 1;
            
            va_list args;
            va_start(args, dims);
            
            for (unsigned long d = 0; d < dims; d++)
            {
                unsigned long shapeThis = va_arg(args, unsigned long);
                
                elems         *= shapeThis;
                this->shape[d] = shapeThis;
            }
            
            va_end(args);
            
            values = (T*) calloc(elems, sizeof(T));
            
            if (values == nullptr)
            {
                cout << "ERROR: Trying to create Tensor with " << to_string(dims) << " dimensions which does not fit in memory.\n";
            }
        }
        
        ~Tensor()
        {
            free(values);
            free(shape);
        }
        
        // Set all elements in this tensor to zero.
        
        void zeros()
        {
            free(values);
            
            values = (T*) calloc(elems, sizeof(T));
        }
        
        // Set all elements in this tensor to a random integer in the range [start, end].
        
        void randint(const int start, const int end)
        {
            unsigned seed = chrono::system_clock::now().time_since_epoch().count();
            
            default_random_engine gen(seed);
            uniform_int_distribution<T> dist(start, end);
            
            #pragma omp parallel for
            for (unsigned long e = 0; e < elems; e++)
            {
                values[e] = dist(gen);
            }
        }
        
        // Apply a function to all elements in this tensor.
        
        void map(T (*func)(const T))
        {
            #pragma omp parallel for
            for (unsigned long e = 0; e < elems; e++)
            {
                values[e] = func(values[e]);
            }
        }
        
        // Sum all elements in this tensor together.
        
        T sum() const
        {
            T sum = 0;
            
            for (unsigned long e = 0; e < elems; e++)
            {
                sum += values[e];
            }
            
            return sum;
        }
        
        // Get the maximum value within this tensor.
        
        T max() const
        {
            T max = values[0];
            
            for (unsigned long e = 1; e < elems; e++)
            {
                if (values[e] > max)
                {
                    max = values[e];
                }
            }
            
            return max;
        }
        
        unsigned long argmax() const
        {
            unsigned long argmax = 0;
            
            for (unsigned long e = 1; e < elems; e++)
            {
                if (values[e] > values[argmax])
                {
                    argmax = e;
                }
            }
            
            return argmax;
        }
        
        // Get the minimum value within this tensor.
        
        T min() const
        {
            T min = values[0];
            
            for (unsigned long e = 1; e < elems; e++)
            {
                if (values[e] < min)
                {
                    min = values[e];
                }
            }
            
            return min;
        }
        
        // Sum all elements in this tensor together, assuming they represent natural logarithms.
        
        float sumLog() const
        {
            float sum = values[0];

            for (unsigned long e = 1; e < elems; e++)
            {
                sum += log1p(exp(values[e] - sum));
            }
            
            return sum;
        }
        
        // Access an element for reading and writing (not thread-safe):
        
        T& operator()(...)
        {
            unsigned long idx    = 0;
            unsigned long stride = 1;
            
            va_list args;
            va_start(args, dims);
            
            for (unsigned long d = 0; d < dims; d++)
            {
                unsigned long indexThis = va_arg(args, unsigned long);
                
                if (indexThis >= shape[d])
                {
                    cout << "ERROR: Trying to access Tensor index " << to_string(indexThis) << " at dimension " + to_string(d + 1) + " with length " + to_string(shape[d]) + "\n";
                    
                    throw;
                }
                
                idx    += indexThis * stride;
                stride *= shape[d];
            }
            
            va_end(args);

            return values[idx];
        }
        
        // Increases an element by one (thread-safe):
        
        void inc(...)
        {
            unsigned long idx    = 0;
            unsigned long stride = 1;
            
            va_list args;
            va_start(args, dims);
            
            for (unsigned long d = 0; d < dims; d++)
            {
                unsigned long indexThis = va_arg(args, unsigned long);
                
                if (indexThis >= shape[d])
                {
                    cout << "ERROR: Trying to access Tensor index " << to_string(indexThis) << " at dimension " + to_string(d + 1) + " with length " + to_string(shape[d]) + "\n";
                }
                
                idx    += indexThis * stride;
                stride *= shape[d];
            }
            
            va_end(args);

            #pragma omp critical (safeUpdate)
            {
                if (values[idx] + (T) 1 < values[idx])
                {
                    cout << "ERROR: Numerical overflow when trying to increase a Tensor element by one.\n";
                }
                else
                {
                    values[idx]++;
                }
            }
        }
        
        // Decreases an element by one (thread-safe):
        
        void dec(...)
        {
            unsigned long idx    = 0;
            unsigned long stride = 1;
            
            va_list args;
            va_start(args, dims);
            
            for (unsigned long d = 0; d < dims; d++)
            {
                unsigned long indexThis = va_arg(args, unsigned long);
                
                if (indexThis >= shape[d])
                {
                    cout << "ERROR: Trying to access Tensor index " << to_string(indexThis) << " at dimension " + to_string(d + 1) + " with length " + to_string(shape[d]) + "\n";
                }
                
                idx    += indexThis * stride;
                stride *= shape[d];
            }
            
            va_end(args);

            #pragma omp critical (safeUpdate)
            {
                if (values[idx] - (T) 1 > values[idx])
                {
                    cout << "ERROR: Numerical underflow when trying to decrease a Tensor element by one.\n";
                }
                else
                {
                    values[idx]--;
                }
            }
        }
};

#endif