#ifndef BASELINE_RANDOM_HPP
#define BASELINE_RANDOM_HPP

#include "TopicModel.hpp"

class BaselineRandom: public TopicModel
{
    protected:
        bool onlyMostLikelyItem;
        
        unsigned long mostLikelyItem;
    
    public:
        BaselineRandom(const bool onlyMostLikelyItem);
        
        ~BaselineRandom();
        
        void train(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum);

        void testExtended(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum, unsigned long* casesNum, unsigned long** casesCount, float** perplexity, float** accuracy);
};

#endif