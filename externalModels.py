import lda, sys

import numpy as np

from tqdm             import tqdm
from random           import randint
from scipy.cluster.vq import vq, kmeans2

BATCH_SIZE = 10000

def loadData(csv, batchSize, numWords, split = False):
    data    = np.zeros((batchSize, numWords), dtype = np.uint8)
    dataBis = np.zeros((batchSize, numWords), dtype = np.uint8)
    
    idxDoc = 0
        
    for _ in range(batchSize):
        idxWords = map(int, csv.readline().split(","))
        
        for idxWord in idxWords:
            if not split or randint(0, 1) == 0:
                data[idxDoc, idxWord]    += 1
            else:
                dataBis[idxDoc, idxWord] += 1
        
        idxDoc += 1
    
    return (data, dataBis)

#model = lda.LDA(n_topics = K, alpha = 1e-4, eta = 1e-4)

#model.fit(dataTraining)

#theta = model.transform(dataTesting)

#probs = theta.dot(model.components_)
        
if __name__ == "__main__":
    filenameCSV = sys.argv[1]
    K           = int(sys.argv[2])
    
    csv = open(filenameCSV, "r")
    
    (numDocs, numWords, numItems) = map(int, csv.readline().split(","))
    
    batchNumTrain = int(numDocs * 0.9) / BATCH_SIZE
    batchNumTest  = int(numDocs * 0.1) / BATCH_SIZE

    centroidsPrev = None

    for batch in tqdm(range(batchNumTrain)):
        (data, _) = loadData(csv, BATCH_SIZE, numWords, split = False)
        
        if batch == 0:
            (centroids, _) = kmeans2(data,         K, minit = 'points')
        else:
            (centroids, _) = kmeans2(data, centroids, minit = 'matrix')
            
            diff = np.sum(np.abs(centroidsPrev - centroids))
            
            if diff == 0:
                break

        centroidsPrev = centroids

    N          = np.zeros((50000, ))
    count      = np.zeros((50000, ))
    perplexity = np.zeros((50000, ))
    accuracy   = np.zeros((50000, ))
    
    for batch in tqdm(range(batchNumTest)):
        (data, dataBis) = loadData(csv, BATCH_SIZE, numWords, split = True)
        
        (code, _) = vq(data, centroids)
        
        probs = np.zeros((BATCH_SIZE, numWords))
        
        for idxDoc in range(BATCH_SIZE):
            probs[idxDoc, :] = centroids[code[idxDoc]] / np.sum(centroids[code[idxDoc]])

            length = np.sum(data[idxDoc]) + np.sum(dataBis[idxDoc])
            
            count[length] += 1
            
            for idxWord in range(probs.shape[1]):
                N[length] += dataBis[idxDoc, idxWord]
                
                if dataBis[idxDoc, idxWord] > 0:
                    perplexity[length] -= dataBis[idxDoc, idxWord] * np.log2(probs[idxDoc, idxWord])
                    accuracy[length]   += dataBis[idxDoc, idxWord] * probs[idxDoc, idxWord] 
        
    csv.close()
        
    print "Perplexity = %f, accuracy = %f" % (2 ** (np.sum(perplexity) / np.sum(N)), np.sum(N) / np.sum(accuracy))
            
    for length in range(50000):
        if count[length] > 0:
            print "Documents with %i items: Count = %i, perplexity = %f, accuracy = %f" % (length, count[length], 2 ** (perplexity[length] / N[length]), N[length] / accuracy[length])