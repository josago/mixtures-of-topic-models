#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This script creates a dataset from the Wikipedia corpora, saving it to disk from time to time.
# Modify the following configuration variables to suit your own needs:

WIKIPEDIA_LANGUAGE = 'en'
WORD_MIN_LENGTH    = 3
WORD_STOP_LIST     = ['about', 'after', 'all', 'already', 'also', 'although', 'always', 'and', 'another', 'any', 'are', 'around', 'back', 'because', 'been', 'before', 'besides', 'between', 'but', 'cannot', 'could', 'despite', 'during', 'ever', 'for', 'forever', 'from', 'has', 'have', 'her', 'hers', 'him', 'his', 'how', 'however', 'just', 'less', 'more', 'never', 'next', 'none', 'not', 'one', 'only', 'other', 'otherwise', 'our', 'ours', 'out', 'over', 'quite', 'same', 'should', 'since', 'some', 'spite', 'still', 'that', 'the', 'their', 'theirs', 'them', 'these', 'they', 'this', 'those', 'though', 'thus', 'too', 'under', 'until', 'very', 'was', 'were', 'what', 'when', 'where', 'whether', 'which', 'whithin', 'who', 'whose', 'why', 'will', 'with', 'without', 'would', 'yes', 'yet']
SAVE_EVERY_DOCS    = 10

import cPickle, os, re, sys, time, wikipedia

from stemming.porter2 import stem

wikipedia.set_lang(WIKIPEDIA_LANGUAGE)

NAMES = {}
WORDS = {}
DOCS  = []

try:
    pkl = open("wikipedia.pkl", "rb")
    
    print "Loading .pkl file...",

    (NAMES, WORDS, DOCS) = cPickle.load(pkl)
    
    pkl.close()
    
    print "OK."
except IOError:
    pass

saving = SAVE_EVERY_DOCS

while True:
    try:
        titles = wikipedia.random(pages = 10)
    except:
        time.sleep(1.0)
    
    for title in titles:
        try:
            doc  = []
            page = wikipedia.page(title)

            if title not in NAMES:
                NAMES[title] = True
                
                tokens = re.split("[\!,;\.:\(\)\-\"\'/]|==|\s+|\d+|\[^\]*\]", page.content)
                
                for token in tokens:
                    word = stem(token).lower()
                    
                    if len(word) >= WORD_MIN_LENGTH and word not in WORD_STOP_LIST:
                        doc.append(word)
                
                if len(doc) > 0:
                    doc_set = set(doc)

                    for word in doc_set:
                        if word in WORDS:
                            WORDS[word] += 1
                        else:
                            WORDS[word]  = 0
                    
                    DOCS.append(doc)
                    
                    saving -= 1

                if saving == 0:
                    print "Saving dataset to disk...",
                    
                    if os.path.isfile("wikipedia.pkl"):
                        os.rename("wikipedia.pkl", "wikipedia.pkl.backup")
                        
                    pkl = open("wikipedia.pkl", "wb")
                    
                    obj = cPickle.dumps((NAMES, WORDS, DOCS))
                    
                    pkl.write(obj)
                    
                    pkl.close()
                    
                    size = sys.getsizeof(obj)
                    
                    if size < 1024:
                        print "OK, " + str(len(obj)) + " bytes written."
                    elif size < 1024 * 1024:
                        print "OK, " + str(len(obj) / 1024) + " KiB written."
                    elif size < 1024 * 1024 * 1024:
                        print "OK, " + str(len(obj) / (1024 * 1024)) + " MiB written."
                    else:
                        print "OK, " + str(len(obj) / (1024 * 1024 * 1024)) + " GiB written."
                        
                    saving = SAVE_EVERY_DOCS
                    
                    print str(len(WORDS)) + " unique words across " + str(len(DOCS)) + " documents."
        except:
            pass