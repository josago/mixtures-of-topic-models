#ifndef TOPIC_MODEL_MIXTURE_HPP
#define TOPIC_MODEL_MIXTURE_HPP

#define TOPIC_MODEL_MIXTURE_GLOBAL 0
#define TOPIC_MODEL_MIXTURE_LDA    1

#include "Tensor.hpp"
#include "TopicModel.hpp"

class TopicModelMixture: public TopicModel
{
    protected:
        unsigned K0, K1;
        
        float alpha0, alpha1, beta0, beta1, pi0;

        Tensor<unsigned>*       N_m;    // Count of items per mode.
        Tensor<unsigned>*       N_mk;   // Count of items per mode and topic.
        Tensor<unsigned>*       N_mkw;  // Count of items per mode, word and topic.
        
        Tensor<unsigned>*       N_d;    // Count of items per document.
        Tensor<unsigned>*       N_md;   // Count of items per mode and document.
        Tensor<unsigned short>* N_mdk;  // Count of items per mode, document and topic.
        Tensor<unsigned short>* N_mdkw; // Count of items per mode, document, topic and word.

        Tensor<unsigned short>* S;      // Mixture component selector variable.
        
        Tensor<unsigned> gibbsSampler(const unsigned N, const unsigned* D, const unsigned* X, const unsigned samplesPerBatch, const bool train);
        
    public:
        TopicModelMixture(const unsigned K0, const unsigned K1, const float alpha0, const float alpha1, const float beta0, const float beta1, const float pi0);
        
        ~TopicModelMixture();
        
        void openCSV(const string filename);
        
        void train(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum);
        
        void testExtended(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum, unsigned long* casesNum, unsigned long** casesCount, float** perplexity, float** accuracy);
};

#endif