#include <cmath>
#include <sstream>
#include <iostream>

#include "TopicModel.hpp"

void TopicModel::openCSV(const string filename)
{
    csv.open(filename);
    
    if (csv.is_open())
    {
        string temp;
        
        getline(csv, temp, ',');
        
        numDocs  = stoi(temp);
        
        getline(csv, temp, ',');
        
        numWords = stoi(temp);
        
        getline(csv, temp, ',');
        
        numItems = stoi(temp);
        
        cout << "Loaded CSV with " + to_string(numDocs) + " documents, " + to_string(numWords) + " unique words and " + to_string(numItems) + " total items.\n";
    }
    else 
    {
        cout << "Could not open file " + filename + " for reading.\n";
    }
}

void TopicModel::loadCSV(const unsigned batchSize, unsigned* N, unsigned** D, unsigned** X, unsigned* Nbis, unsigned** Dbis, unsigned** Xbis)
{
    *N = 0;
    
    if (Nbis != nullptr)
    {
        *Nbis = 0;
    }
    
    unsigned readDocs = 0;
    
    while (readDocs < batchSize)
    {
        string line;
        
        getline(csv, line);
        
        istringstream doc(line);
        
        string word;
        
        while (getline(doc, word, ','))
        {
            unsigned short set = rand() % 2;
            
            if (Nbis == nullptr or Xbis == nullptr)
            {
                set = 0;
            }
            
            if (set == 0)
            {
                (*N)++;
            
                *X = (unsigned*) realloc(*X, *N * sizeof(unsigned));
            
                (*X)[*N - 1] = stoi(word);
                
                if (D != nullptr)
                {
                    *D = (unsigned*) realloc(*D, *N * sizeof(unsigned));
                    
                    (*D)[*N - 1] = readDocs;
                }
            }
            else
            {
                (*Nbis)++;
            
                *Xbis = (unsigned*) realloc(*Xbis, *Nbis * sizeof(unsigned));
            
                (*Xbis)[*Nbis - 1] = stoi(word);
                
                if (Dbis != nullptr)
                {
                    *Dbis = (unsigned*) realloc(*Dbis, *Nbis * sizeof(unsigned));
                    
                    (*Dbis)[*Nbis - 1] = readDocs;
                }
            }
        }

        readDocs++;
    }
}

void TopicModel::test(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum, float* perplexity, float* accuracy)
{
    unsigned long  casesNum;
    unsigned long* casesCount = nullptr;
    
    float* perplexityExtended = nullptr;
    float* accuracyExtended   = nullptr;
    
    testExtended(batchSize, samplesPerBatch, batchNum, &casesNum, &casesCount, &perplexityExtended, &accuracyExtended);
    
    unsigned long Nall = 0;
    
    for (unsigned long i = 0; i < casesNum; i++)
    {
        Nall += casesCount[i];
    }
    
    *perplexity = 0.0;
    *accuracy   = 0.0;
    
    for (unsigned long i = 0; i < casesNum; i++)
    {
        if (casesCount[i] > 0)
        {
            *perplexity += (casesCount[i] / (float) Nall) * perplexityExtended[i];
            *accuracy   += (casesCount[i] / (float) Nall) * accuracyExtended[i];
        }
    }
    
    free(casesCount);
    free(perplexityExtended);
    free(accuracyExtended);
}