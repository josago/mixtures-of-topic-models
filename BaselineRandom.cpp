#include "Tensor.hpp"
#include "BaselineRandom.hpp"

BaselineRandom::BaselineRandom(const bool onlyMostLikelyItem)
{
    this->onlyMostLikelyItem = onlyMostLikelyItem;
}
        
BaselineRandom::~BaselineRandom()
{
    // Do nothing.
}
        
void BaselineRandom::train(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum)
{
    Tensor<unsigned long> N_x(1, numWords);
    
    N_x.zeros();
    
    for (unsigned b = 0; b < batchNum; b++)
    {
        // Make the batches, but otherwise do nothing.
        
        cout << "Batch #" + to_string(b + 1) + "...\n";
    
        unsigned  N;
        unsigned* X = nullptr;
    
        loadCSV(batchSize, &N, nullptr, &X, nullptr, nullptr, nullptr);
        
        if (onlyMostLikelyItem)
        {
            for (unsigned i = 0; i < N; i++)
            {
                N_x.inc(X[i]);
            }
        }
        
        free(X);
    }
    
    if (onlyMostLikelyItem)
    {
        mostLikelyItem = N_x.argmax();
    }
}

void BaselineRandom::testExtended(const unsigned batchSize, const unsigned samplesPerBatch, const unsigned batchNum, unsigned long* casesNum, unsigned long** casesCount, float** perplexity, float** accuracy)
{
    *casesNum   = 0;
    *casesCount = nullptr;
    
    Tensor<unsigned long> N_d(1, batchSize);
    
    for (unsigned b = 0; b < batchNum; b++)
    {
        cout << "Batch #" + to_string(b + 1) + "...\n";
            
        // Get a new batch:
        
        unsigned  N;
        unsigned* X = nullptr;
        unsigned* D = nullptr;
    
        loadCSV(batchSize, &N, &D, &X, nullptr, nullptr, nullptr);
        
        // Calculate the number of items per document:
        
        N_d.zeros();

        for (unsigned i = 0; i < N; i++)
        {
            N_d(D[i])++;
        }
        
        // Enlarge data structures as needed:
        
        const unsigned long casesNumOld = *casesNum;
        
        *casesNum = N_d.max() + 1;
        
        *casesCount = (unsigned long*) realloc(*casesCount, *casesNum * sizeof(unsigned long));
        *perplexity = (float*)         realloc(*perplexity, *casesNum * sizeof(float));
        *accuracy   = (float*)         realloc(*accuracy,   *casesNum * sizeof(float));
        
        for (unsigned j = casesNumOld; j < *casesNum; j++)
        {
            (*casesCount)[j] = 0;
            (*perplexity)[j] = 0.0;
            (*accuracy)[j]   = 0.0;
        }
        
        // Aggregate all partial metrics:
        
        #pragma omp parallel for
        for (unsigned i = 0; i < N; i++)
        {
            const unsigned d = N_d(D[i]);
            
            float prob = 1.0 / numWords;
            
            if (onlyMostLikelyItem)
            {
                if (X[i] == mostLikelyItem)
                {
                    prob = 1.0;
                }
                else
                {
                    prob = 0.0;
                }
            }
            
            #pragma omp atomic
            (*casesCount)[d]++;
            
            #pragma omp atomic
            (*perplexity)[d] += log2(prob);
            
            #pragma omp atomic
            (*accuracy)[d]   += prob;
        }
        
        free(X);
        free(D);
    }
    
    // Get the final metrics:
    
    #pragma omp parallel for
    for (unsigned d = 0; d < *casesNum; d++)
    {
        (*perplexity)[d] = pow(2, - (*perplexity)[d] / (*casesCount)[d]);
        (*accuracy)[d]   = (*accuracy)[d] / (*casesCount)[d];
    }
}