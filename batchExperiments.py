import os, numpy

BATCH_DOCUMENTS_NUM   = 1000
GIBBS_ITERATIONS      = 100
DOCUMENTS_PER_DATASET = {'recipes': (101137, 11237), 'market': (5558545, 617616), 'wikipedia': (54143, 6015)}

os.system("mkdir out 2> /dev/null")

for mode in ('4', ): #('0', '1', '2', '3', '4'):
    if mode in ('0', '1'):
        listK = (0, )
    else:
        listK = (10, 20, 50, 100)
        
    for K in listK:
        for dataset in ('recipes', ): # 'market', 'wikipedia'):
            if mode in ('0', '1'):
                iterations = 1
            else:
                iterations = GIBBS_ITERATIONS
            
            trainingBatches = int(numpy.floor(float(DOCUMENTS_PER_DATASET[dataset][0]) / BATCH_DOCUMENTS_NUM))
            testingBatches  = int(numpy.ceil(float(DOCUMENTS_PER_DATASET[dataset][1]) / BATCH_DOCUMENTS_NUM))
            
            os.system("./mixtures csv/%s.csv %s %d %d %d %d %d > out/mixtures_%s_%s_K%d.log" % (dataset, mode, K, BATCH_DOCUMENTS_NUM, trainingBatches, testingBatches, iterations, mode, dataset, K))
