\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{5}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Background}{7}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Market Basket Analysis}{7}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Rule-extracting algorithms}{8}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}K-means}{9}{subsection.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}Latent Dirichet Allocation}{9}{subsection.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5}Variations on Latent Dirichlet Allocation}{11}{subsection.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Approach}{13}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Selected domains}{13}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1}Wikipedia articles}{13}{subsubsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2}Market basket analysis}{14}{subsubsection.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.3}Food recipes}{16}{subsubsection.3.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Model definition}{17}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1}Corpus-specific distributions}{18}{subsubsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.2}Document-specific distributions}{21}{subsubsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.3}Mixed distributions}{23}{subsubsection.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.4}Inference}{26}{subsubsection.3.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.5}Training}{28}{subsubsection.3.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.2.5.1}Corpus-specific distributions}{29}{paragraph.3.2.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.2.5.2}Document-specific distributions}{32}{paragraph.3.2.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.2.5.3}Mixed distributions}{32}{paragraph.3.2.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Model implementation}{36}{subsection.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}Model evaluation}{39}{subsection.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.1}Perplexity}{39}{subsubsection.3.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.2}Inverse accuracy}{40}{subsubsection.3.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Experiments}{41}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}LDA versus baselines}{41}{subsection.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}LDA performance versus container length}{41}{subsection.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}LDA versus mixture of topic models}{43}{subsection.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Discussion of results}{51}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Conclusions}{54}{section.6}
